---
title: "Como relatar bugs oficialmente"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

## Como relatar bugs oficialmente

1) Instale o pacote reportbug:

$ aptitude install reportbug

2) Execute o utilitário reportbug usando a sintaxe a seguir:

$ reportbug <nome_pacote_com_bug\>

Por exemplo, para relatar um bug no pacote "hello", a sintaxe seria:

$ reportbug hello

Caso o pacote para o qual você esteja relatando o bug não esteja instalado na
máquina a partir de onde o bug está sendo relatado, o reportbug lhe informará
sobre esse detalhe e lhe questionará se você deseja realmente relatar o bug,
como pode ser visto a seguir:

andrelop@foolish: ~/po-debconf/cupsys$ reportbug cupsys

Using '"Andre Luis Lopes" <andrelop@debian.org>' as your from address.

Detected character set: ISO-8859-1

Please set your locale if this is incorrect.

Getting status for cupsys...

A package named "cupsys" does not appear to be installed; do you want to search
for a similar-looking filename in an installed package [Y|n|?]?

Tecle 'y' (a letra y, sem as aspas) para que o reportbug procure por um arquivo
dentro de um pacote com um nome similar.

Finding package for 'cupsys'...

Using package 'libcupsys2'.

A package named 'cupsys' does not appear to be installed on your system;
however, 'libcupsys2' contains a file named 'cupsys'. Do you want to file your
report on the package reportbug found [Y|n|?]

Se desejar informar algum bug com o pacote encontrado, tecle 'y' e continue o
processo, ou 'n' para sair. No primeiro prompt, se digitar 'n', o reportbug
pedirá que informe a versão do pacote ao qual você quer informar o bug. Tome
cuidado pois você se você digitar um pacote que não existe, sua notificação de
bug talvez seja perdida, a mesmo que esteja notificando o desejo e empacotar um
novo pacote. Veja o que acontece quando digitamos 'n':

Please enter the version of the package this report applies to (blank OK)

>

Informe a versão do pacote após o prompt iniciado com '>' exibido acima.

Importante: Quando estiver enviando uma tradução de um template debconf
(recomendada-se que seja um bug de severidade "wishlist" - "lista de desejos")
é extremamente importante informar o número de versão do pacote ao qual a
tradução se aplica. Para auxiliá-lo nesse caso, as traduções parciais, as quais
podem ser obtidas em <http://www.debian.org/intl/l10n/po/pt_BR>, possuem todas
elas, como parte do nome do arquivo parcialmente traduzido, o número de versão
do pacote. Um exemplo aleatório, somente para fins didáticos, seria o arquivo
cupsys_1.1.20final+cvs20040330-4_pt_BR.po.gz, obtido através do link
http://gluck.debian.org/~pmachard/l10n/material/po/unstable/main/c/cupsys/debian/po/cupsys_1.1.20final+cvs20040330-4_pt_BR.po.gz.
Como podemos constatar, o arquivo contendo os templates parcialmente traduzidos
claramente se aplica a versão 1.1.20final+cvs20040330-4 do pacote cupsys.

Como próximo passo, o reportbug consultará o sistema de acompanhamento de bugs
Debian (Debian BTS, ou Debian Bug Tracking System) e listará os bugs atualmente
existentes e relatados para o pacote em questão. Devido a isso, é recomendado
executar o reportbug quando estiver conectado a Internet. Existem maneiras de
executar o reportbug e desabilitar a consulta de bugs relatados no Debian BTS,
mas o recomendado é sempre manter essa opção habilitada (o que é o padrão) para
certificar-se de não estar relatando novamente um bug já relatado.

Como exemplo, podemos ver abaixo uma amostra parcial dos bugs relatados para o
pacote cupsys (novamente, um pacote aleatório escolhido somente para fins
didáticos):

Querying Debian bug tracking system for reports on cupsys 116 bug reports found:

Important bugs - outstanding

1) #170399: cupsys web interface

2) #171320: cupsys: Changing from standard to floyd-steinberg causes wrong paper
size to be used

3) #193476: cupsys: Upgrade severely breaks PPD handling

4) #196007: does not print to lpd:// destination

5) #198803: cupsys: cupsd startup changes group of SSL certificate and key without asking

6) #201387: cupsys: fails to print documents containing multiple %%EOF lines

7) #211677: cupsys restart hangs when upgrading testing

8) #215468: cupsys tries to use malformed device URIs

9) #219892: cupsys: Fails to print to local epson printer

10) #225647: cupsys: Inserting printers results in lpadmin spinning

(1-10/116) Is the bug you found listed above [y|N|m|r|q|s|f|?]?

O reportbug nos traz a informação que o cupsys possui atualmente 116 bugs
relatados. O reportbug inicia listando os bugs de severidade "important" e cada
bug é listado com seu número único, logo após o símbolo '#', que o identifica no
Debian BTS, seguido por uma pequena descrição do bug. Ao final da listagem
parcial, o reportbug lhe questiona se o bug que você encontrou está listado
entre os bugs trazidos pela listagem parcial exibida.

É importante frisar que relatar o mesmo bug diversas vezes têm na verdade o
efeito contrário do esperado, ou seja, o mantenedor do pacote gastará mais tempo
tendo que lidar com os relatórios de bugs recebidos, tempo este que poderia
estar sendo gasto corrigindo o problema já relatado. Sendo assim, por mais
tentador que lhe pareça, não relate um novo bug para um problema que já é
conhecido e foi relatado anteriormente.

Por outro lado, caso você tenha qualquer acréscimo a fazer aos detalhes do
relatório de bug original, mesmo caso não tenha sido você quem relatou o bug originalmente, é interessante enviar seus comentários sobre o bug. Comentários
úteis são aqueles que auxiliam o mantenedor do pacote a reproduzir o bug, contém
detalhes de seu ambiente e em quais circunstâncias o bug foi observado. Não
envie comentários para um relatório de bug existente somente para pressionar o
mantenedor do pacote e apressá-lo para corrigir o bug. Lembre-se que todos os participantes do projeto Debian contribuem de forma voluntária e cada pessoa
possui seu próprio tempo para lidar com os assuntos relacionados ao projeto,
incluíndo correção de bugs.

Caso você encontre nessa listagem um bug já relatado para o problema que você
encontrou, somente responda 'y' e informe o número do bug quando o reportbug
perguntar.

(1-10/116) Is the bug you found listed above [y|N|m|r|q|s|f|?]? y

Enter the number of the bug report you want to give more info on,
or press ENTER to exit: #170399

Please provide a subject for your response; no subject will stop reportbug.

>

Se desejar ver mais detalhes de um determinado bug antes de enviar os seus
comentários, digite o número do bug no prompt onde pergunta se o bug está
listado (o prompt que respondi 'y' agora a pouco) e assim você olhará este bug.
Para sair da visualização digite 'q'. Se o bug tiver algum comentário, você pode
acompanhar a discussão digitando 'n' para ir ao próximo comentário ou 'p' para
voltar ao comentário anterior. Desejando adicionar seu comentário, digite 'x'
que o reportbug irá encaminhá-lo na forma de como proceder. Caso o bug não
corresponda ao que esperava, digite 'o' para voltar a tela que lista os bugs
existentes. Abaixo um exemplo de como visualizar mais informaçoes sobre um bug.

(1-10/116) Is the bug you found listed above [y|N|m|r|q|s|f|?]? 170399

Retrieving report #170399 from Debian bug tracking system...

What do you want to do now? [N|x|o|r|b|q|?]? n

What do you want to do now? [p|x|O|r|b|q|?]? x

Please provide a subject for your response; no subject will stop reportbug.

>

Você terá agora a possibilidade de enviar seus comentários dando maiores
informações que auxiliarão o mantenedor do pacote a corrigir o bug.

O reportbug irá, agora, invocar seu cliente de e-mail (MUA, ou Mail User Agent)
preferido para que você possa compor a mensagem com os detalhes do bug a ser
enviado ao Debian BTS. Seu MUA preferido pode ser especificado no arquivo de
configuração global do reportbug, localizado em /etc/reportbug.conf, ou em um
arquivo de configuração específico com as preferências do usuário particular
utilizador do reportbug, localizado em $HOME/.reportbugrc e que pode ser
configurado facilmente com 'reportbug --configure'. O arquivo de configuração
global possui várias instruções comentadas como exemplo, facilitando localizar
quais parâmetros devem ser configurados caso você queira alterar a configuração
padrão.

Caso o bug que você tenha encontrado não esteja listado na listagem parcial
trazida pelo reportbug, simplesmente responda 'n' para que o reportbug continue
exibindo o restante dos bugs encontrados e relatados para o pacote em questão.
Repare que, por padrão, o reportbug lista os bugs existentes para o pacote
organizados por severidade, sendo assim, somente bugs de severidade mais alta
serão listados inicialmente.

Caso você esteja procurando na listagem por um possível bug relatado contendo
uma atualização de tradução relacionada a um possível template debconf existente
no pacote em questão, é extremamente importante continuar verificando a listagem
de bugs trazidos pelo reportbug, uma vez que os bugs relacionados a templates
debconf são relatados com severidade "wishlist" e, logicamente, tais bugs
possuem uma severidade menor, sendo portanto listados mais ao final da listagem
trazida pelo reportbug.

Caso você examine toda a listagem trazida pelo reportbug e não encontre nenhum
bug relatado semelhante ao bug que você encontrou, relate o bug encontrado como
um novo bug. Você conseguirá fazê-lo verificando a listagem de bugs trazida pelo
reportbug até o final e respondendo 'n' para todas as listagens parciais que
forem exibidas. Quando você responder 'n' para a última listagem, o reportbug
irá lhe pedir par descrever abreviadamente o bug encontrado.

Fique atento para não exagerar na descrição mínima requisitada pelo reportbug
pois tal descrição será usada como título do novo bug sendo relatado.
Pessoalmente, procuro manter os títulos dos bugs que relato com menos de 75
caracteres. Adicionalmente, caso esteja relatando um bug relacionado a tradução
de templates debconf, procure sempre incluir no título do relatório do bug as
marcas '[INTL:pt_BR]'. Tais marcas destacam o bug e permitem que o mantenedor
do pacote reconheça facilmente que o bug é relacionado a internacionalização
(daí o nome INTL) para o idioma português do Brasil (pt_BR é o código de nosso
idioma).

Um exemplo de título de um relatório de bug usado para enviar um patch contendo
uma atualização de tradução relacionada a um template debconf seria:

[INTL:pt_BR] Please consider applying the attached updated debconf template
translation

Similarmente, um exemplo de título de bug usado para enviar uma nova tradução,
e não um patch para atualizar uma tradução já existente, relacionada a um
template debconf seria:

[INTL:pt_BR] Please consider adding the attached debconf template translation

Após informar o título do bug, o próximo passo é escolher a severidade do bug.
É importante que você seja realmente honesto aqui e escolha a severidade real do
bug de acordo com a descrição de cada severidade exibida pelo reportbug. Existe
um fenômeno conhecido no projeto Debian como "inflar bugs". Um bug geralmente
em sua severidade "inflada", ou seja, distorcida, quando é elevada na esperança
de que o mantenedor do pacote ao qual o bug se aplica dê maior atenção ao bug e
o corrija antes dos demais bugs. Novamente, o efeito conseguido nesse caso é o
inverso. O mantenedor entenderá que o bug na verdade não possui uma severidade
tão elevada como a severidade que você atribuiu ao mesmo e irá "desinflar" a
severidade rebaixando o bug para uma severidade correta. Para evitar guerras
travadas no Debian BTS, com o mantenedor rebaixando e você inflando novamente a
severidade de um bug indefinidamente, seja sincero desde o início e atribua a
severidade correta ao bug sendo relatado mesmo que isso vá contra seus
interesses pessoais em ver o bug corrigido rapidamente.

Segue um exemplo do momento em que o reportbug lhe questiona sobre a severidade
do bug sendo relatado:

How would you rate the severity of this problem or report?

1 normal a bug that does not undermine the usability of the whole package; for
example, a problem with a particular option or menu item.

2 minor things like spelling mistakes and other minor cosmetic errors that do
not affect the core functionality of the package.

3 wishlist suggestions and requests for new features.

Please select a severity level: [normal]

Perceba que, caso você não indique a severidade do bug, por padrão a severidade
assumida é "normal". Como já citado anterirmente, a severidade correta para bugs
que sejam relacionados a traduções de templates debconf é a severidade
"wishlist".

Como a severidade é um aspecto importante do processo de relato de bug, segue
abaixo uma tradução livre, não oficial das descrições das severidades listadas
pelo reportbug:

1 normal um bug que não atrapalha a usabilidade de todo o pacote; por exemplo,
um problema com uma opção ou item de menu em particular.

2 minor aspectos como erros de ortografia e outros erros cosméticos que não
afetam a funcionalidade principal do pacote.

3 wishlist sugestões e requisições de novos recursos.

Repare que o reportbug somente lista como severidades possíveis as severidades
normal, minor e wishlist, usando como padrão a severidade "normal" devido a
grande maioria dos bugs relatados serem na verdade de severidade normal. Existem
outras severidades mais elevadas, tais como "important", "serious", "grave" e
"critical". Tais severidades podem ter sua descrição verificada na documentação
para desenvolvedores do sistema de acompanhamento de bugs Debian, mais
especificamente em <http://www.debian.org/Bugs/Developer.pt.html#severities>.

Após informar a severidade correta, o reportbug irá invocar seu cliente de
e-mail preferido para que você possa compor o texto do relatório de bug. Algo
extremamente importante a ser lembrado é que todo o relatório de bug enviado
deve ser escrito em inglês americano e não em português do Brasil. Isso é
necessário porque o idioma oficial usado no projeto Debian é o inglês. O projeto
Debian tem como participantes pessoas de diversos países distintos, cada qual
com seu próprio idioma. Não é correto exigir de um mantenedor de pacotes Debian
que o mesmo possa ler relatórios de bugs em qualquer um dos inúmeros idiomas
existentes e, devido a isso, o projeto utiliza o inglês como idioma oficial
para comunicação.

Voltando ao reportbug, se você acompanhou todos os passos corretamente, agora é
o momento de você realmente escrever o relatório de bugs. Seja claro, descreva
cuidadosamente todos os passos que você executou até conseguir chegar ao
comportamente incorreto do programa em questão, cite também maneiras de como
reproduzir o bug e quaisquer outros detlhes que considere importantes.

Em meu caso, utilizo o MUA "mutt", o qual, por sua vez, utiliza como editor de
textos padrão o editor de textos "vim". Levando esses detalhes em consideração,
segue abaixo um exemplo da tela do editor de textos vim com o esqueleto do
relatório de bug criado pelo reportbug aberto e aguardando minha descrição
detalhada do bug:

Subject: cupsys: [INTL:pt_BR] Please apply attached Brazilian Portuguese (pt_BR)
debconf template translation

Package: cupsys

Version: 1.1.10final-8

Severity: wishlist

* * * Please type your report below this line * * *

-- System Information:

Debian Release: testing/unstable

Architecture: i386

Kernel: Linux foolish 2.6.1 #5 Fri Jan 9 23:11:24 BRST 2004 i686

Locale: LANG=pt_BR, LC_CTYPE=pt_BR (ignored: LC_ALL set to pt_BR)

Vejamos as informações que o reportbug já preencheu em nosso relatório de bugs:
o título do bug sendo relatado, o nome do pacotes, a versão do pacote e a
severidade do bug nos campos "Subject", "Package", "Version" e "Severity", respectivamente.

Após isso, temos a linha:

* * * Please type your report below this line * * *

que explicitamente lhe pede para digitar seu relatório desta linha para baixo.
Essa linha serve para separar o cabeçalho do corpo da mensagem. O seu bug deve
vir abaixo dela.

Após isso, temos mais algumas informações. São elas: a versão do Debian sendo
utilizada, a arquitetura da máquina, a versão do kernel e a configuração de
locales atual nos campos "Debian Release", "Architecture", "Kernel" e finalmente
"Locale", respectivamente.

Estas informações em conjunto com o relatório de bugs que você irá escrever são
o suficiente para permitir que o mantenedor do pacote em questão possa lidar
corretamente com o bug que você está relatando. No entanto, você pode auxiliar
o mantenedor do pacote ainda mais. Adicionalmente aos campos exibidos pelo
esqueleto de relatório de bugs criado pelo reportbug, campos adicionais podem
ser adicionados ao seu relatório de bug. Pessoalmente, um campo que costumo
incluir em meus relatórios de bugs é o campo "Tags". Com esse campo, podemos
atribuir tags ao bug para auxiliar o mantenedor do pacote a organizar melhor o
bug e até mesmo dar uma atenção maior ao nosso bug em alguns casos. Um exemplo
disso é o caso em que você envia um patch para a correção do bug e inclui
"patch" no campo "Tags" do bug para informar ao mantenedor do pacote sobre a
disponibilidade do patch em questão.

No entanto, devido a esses campos adicionais fazerem parte do cabeçalho da
mensagem e não do corpo da mesma, é necessário incluí-lo antes da linha em
branco entre o cabeçalho da mensagem e a linha que lhe indica onde seu relatório
pode começar a ser digitado.

A seguir temos um exemplo de um relatório de bug já preenchido e com o novo
campo sendo usado para auxiliar o mantenedor do pacote em questão:

Subject: cupsys: [INTL:pt_BR] Please apply attached Brazilian Portuguese (pt_BR)
debconf template translation

Package: cupsys

Version: 1.1.10final-8

Severity: wishlist

Tags: sid, patch

* * * Please type your report below this line * * *

Please consider using the attached cupsys's Brazilian Portuguese (pt_BR)
debconf template translation. It was properly checked against errors using the
msgfmt utility from gettext package as can be see bellow

andrelop@foolish:~/po-debconf/cupsys$ msgfmt -o /dev/null -v pt_BR.po

6 mensagens traduzidas.

andrelop@foolish:~/po-debconf/cupsys$

Also, it's gziped for size optimization.

Regards,

-- System Information:

Debian Release: testing/unstable

Architecture: i386

Kernel: Linux foolish 2.6.1 #5 Fri Jan 9 23:11:24 BRST 2004 i686

Locale: LANG=pt_BR, LC_CTYPE=pt_BR (ignored: LC_ALL set to pt_BR)

Após criar o relatório de bug, o reportbug entrará em ação novamente e lhe
apresentará diversas opções. A seguir, temos um exemplo do prompt do reportbug
lhe apresentando diversas opções do que pode ser feito com o relatório de bugs
recém-criado:

Report will be sent to "Debian Bug Tracking System

<submit@bugs.debian.org>

Submit this report on cupsys (e to edit) [y|n|a|c|E|i|l|m|p|?]?

Para verificar todas as opções disponíveis, no prompt do reportbug, digite '?'
(sinal de interrogação, sem as aspas) e tecle <enter\>. Veja as opções que o
reportbug nos oferece:

Submit this report on cupsys (e to edit) [y|n|a|c|E|i|l|m|p|?]? ?

y - Submit the bug report via email.

n - Don't submit the bug report; instead, save it in a temporary file.

a - Attach a file.

c - Change editor and re-edit.

E - (default) Re-edit the bug report.

i - Include a text file.

l - Pipe the message through the pager.

m - Choose a mailer to edit the report.

p - Print message to stdout.

? - Display this help.

Submit this report on cupsys (e to edit) [y|n|a|c|E|i|l|m|p|?]?

De todas as opções disponíveis, uma que nos interessa bastante é a opção "a",
que nos permite anexar um arquivo ao relatório de bug. Em nosso exemplo, estamos
relatando um bug de severidade "wishlist" com uma atualização de tradução para o
pacote "cupsys". Informamos no corpo da mensagem contendo o relatório de bug que
a tradução encontra-se em anexo, por isso, precisamos realmente anexar a
tradução.

Digite 'a' e o prompt a seguir é exibido:

Choose a file to attach:

Aqui, devemos informar ao reportbug o caminho completo para o arquivo contendo
nossa tradução. O reportbug anexará o arquivo e retornará ao prompt, onde você
tem a possibilidade de anexar mais arquivos ou simplesmente enviar a mensagem,
como a seguir:

Report will be sent to "Debian Bug Tracking System

<submit@bugs.debian.org>

Attachments:

/home/andrelop/po-debconf/cupsys/pt_BR.po.gz

Submit this report on cupsys (e to edit) [y|n|a|c|E|i|l|m|p|?]?

Importante: Como visto acima, no caso de traduções de templates debconf, o
arquivo contendo as traduções deve ser renomeado para pt_BR.po e, para
economizar espaço no sistema de acompanhamento de bugs Debian, ser compactado preferencialmente usando o gzip com a flag de otimização -9 (exemplo: gzip -9
pt_BR.po)

Para enviar o relatóriode bug, simplesmente digite 'y'. O relatório de bug,
junto com quaisquer arquivos que tenham sido anexados ao mesmo, será enviado
corretamente ao sistema de acompanhamento de bugs do Debian. Dentro de alguns
minutos (ou de algumas hora) você receberá um e-mail de confirmação lhe
informando que seu relatório de bugs foi recebido e processado pelo Debian BTS,
o mantenedor do pacote irá receber uma cópia do relatório de bug enviado ao BTS
e um número único dentro do sistema será atribuído ao seu bug.

A partir de agora, você poderá verificar o andamento da correção do bug através
da interface Web, usando o endereço http://bugs.debian.org/<número_bug\>.
Adicionalmente, quaisquer detalhes adicionais que você queira informar sobre o
bug poderão ser enviados para o endereço de e-mail <número_bug\>@bugs.debian.org.
Ao enviar qualquer informação adicional para esse endereço de e-mail, tal
informação será armazenada no BTS Debian e qualquer pessoa poderá acompanhar o
andamento da resolução do mesmo.