---
title: "MiniDebConfs no Brasil"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

# MiniDebConfs

As MiniDebConfs são eventos abertos a todos(as), independente do seu nível de
conhecimento sobre Debian. O mais importante é reunir a comunidade para
celebrar o maior projeto de Software Livre no mundo, por isso recebemos desde
usuários(as) inexperientes que estão iniciando o seu contato com o Debian até
mantenedores(as) e desenvolvedores(as) oficiais do projeto.
Ou seja,todoas as pessoas são muito bem-vindas!

As MiniDebConfs são encontros locais organizados por membros do Projeto Debian
para atingir objetivos semelhantes aos da [DebConf](http://debconf.org), mas em
um contexto regional. Durante todo o ano são organizadas MiniDebConfs ao redor
do mundo, como se pode ver [nesta página](https://wiki.debian.org/MiniDebConf).

As MiniDebConfs são compostas por palestras, debates, oficinas, sprints, BSP
(Bug Squashing Party) e eventos sociais.

Veja abaixo as edições das Micro e MiniDebConfs já realizadas no Brasil.

## MicroDebConf Brasília 2018

* Data: 08/09/2018
* Local: Brasília - DF
* Fotos: <https://www.facebook.com/debianbrasiliaoficial/posts/458865631288234>
* Site: <https://microdebconf.debianbrasilia.org>

## MiniDebConf Curitiba 2018

* Data: 11 a 14/04/2018
* Local: Curitiba - PR
* Fotos: <https://www.flickr.com/photos/curitibalivre/albums/72157690015019780>
* Palestras no peertube: <https://peertube.debian.social/videos/watch/playlist/83e9adc3-85b9-4b57-9134-fd11a9a4f6a6>
* Palestras no youtube: <https://www.youtube.com/watch?v=V209L07y2lw&list=PLU90bw3OpxLqlaM2D4iNi4n8J5480ghd0>
* Site: <https://minidebconf.curitiba.br/2018>

## MiniDebConf Curitiba 2017

* Data: 17 a 19/03/2017
* Local: Curitiba - PR
* Fotos: <https://www.flickr.com/photos/curitibalivre/albums/72157679698906961>
* Palestras no peertube: <https://peertube.debian.social/videos/watch/playlist/cf8f5f6e-f4b0-4ab9-9645-942b5cb2fde1>
* Palestras no youtube: <https://www.youtube.com/watch?v=48jVIX6sgOo&list=PLU90bw3OpxLoKKXpGVpGzEiITDDnHzgAu>
* Site: <http://br2017.mini.debconf.org>

## MiniDebConf na 13ª Latinoware em 2016

* Data: 19 e 20/10/2016
* Local: Foz do Iguaçú - PR
* Fotos: <https://debianbrasil.org.br/fotos/fotos-na-latinoware-2016>
* Site: <https://wiki.debian.org/Brasil/Eventos/MiniDebConfLatinoware2016>

## MiniDebConf Curitiba 2016

* Data: 05 e 06/03/2016
* Local: Curitiba - PR
* Fotos: <https://www.flickr.com/photos/curitibalivre/albums/72157665695548695>
* Site: <http://br2016.mini.debconf.org>

## MiniDebConf na 12ª Latinoware em 2015

* Local: Foz do Iguaçu - PR
* Data: 15/10/2015
* Site: <https://wiki.debian.org/Brasil/Eventos/MiniDebConfLatinoware2015>

## MiniDebConf no FISL16 em 2015

* Data 10/07/2015
* Local: Porto Alegre - RS
* Fotos: <https://www.flickr.com/photos/curitibalivre/sets/72157653569094123>
* Palestras no peertube <https://peertube.debian.social/videos/watch/playlist/451b4efc-2fce-4478-a539-33ff20fcfb19>
* Palestras no youtube: <https://www.youtube.com/watch?v=zZ0B8lgtdkc&ist=PLU90bw3OpxLrzqwZmzu3CrWum5ToqF3DO>
* Site: <https://wiki.debian.org/Brasil/Eventos/MiniDebConfFISL2015>

## MicroDebConf Brasília 2015

* Data: 31/05/2015
* Local: Brasília - DF
* Site <https://wiki.debian.org/Brasil/Eventos/MicroDebConfBSB2015>