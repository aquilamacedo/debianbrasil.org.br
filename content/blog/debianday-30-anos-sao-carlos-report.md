---
title: "Debian Day 30 years in São Carlos - Brazil"
kind: article
created_at: 2023-08-20 20:00
author: Carlos Henrique Lima Melara (Charles)
---
This year's [Debian day](https://wiki.debian.org/DebianDay/) was a pretty special one, we are celebrating 30 years!
Giving the importance of this event, the Brazilian community planned a very
special week. Instead of only local gatherings, we had a week of online talks
streamed via Debian [Brazil's youtube channel](https://www.youtube.com/playlist?list=PLU90bw3OpxLoiEUdWm7uyTi71ATGk6oXm) (soon the recordings will be
uploaded to [Debian's peertube instance)](https://peertube.debian.social/w/p/r1gZyzq3anFaECh6QEmU4d). Nonetheless the local celebrations
happened around the country and I've organized one in São Carlos with the help
of [GELOS](https://gelos.club), the FLOSS group at [University of São Paulo](https://www5.usp.br/).

The event happened on August 19th and went on the whole afternoon. We had some
talks about Debian and free software (see table below), a coffee break where we
had the chance to talk, and finished with a group photo (check this one and
many others below). Actually, it wasn't the end, we carried on the conversation
about Debian and free software in a local bar :-)

We had around 30 people in the event and reached a greater audience via the
announcements across the university's press releases and emails sent to our
Brazilian mailing lists. You can check some of them below.

- [Local campus press release](http://www.saocarlos.usp.br/evento-celebra-os-30-anos-de-um-projeto-que-revolucionou-o-mundo-do-software-livre/)
- [Computing institute press release](https://www.icmc.usp.br/noticias/6242-evento-celebra-os-30-anos-de-um-projeto-que-revolucionou-o-mundo-do-software-livre)
- [Engineering institute press release](https://eesc.usp.br/noticias/comunicados_s.php?guid=debian-day-30-anos-acontecera-na-usp-de-sao-carlos&termid=todos)
- [Federal institute press release](https://scl.ifsp.edu.br/index.php/component/content/article/17-ultimas-noticias/2249-divulgacao-debian-day-30-anos-acontecera-na-usp-sao-carlos.html)
- [Debian-br-eventos email](https://alioth-lists.debian.net/pipermail/debian-br-eventos/2023-August/000924.html)

Below you can check the list of talks.

Time | Author | Title
-----|--------|------
14:10 | GELOS | Intro to GELOS
14:30 | Carlos Melara (Charles) | A ~~not so~~ Brief Introdution to the Debian Project
15:15 | Guilherme Paixão | Debian and the Free Culture
15:45 | zé | Free Software: the paths to a free life
16:15 | -- | Coffee Break
17:15 | Prof. Dr. Francisco José Monaco | The FOSS Ecosystem and You

Here are some photos taken during the event!

![Preparation for Debian Day](/blog/imagens/debian-day-30-anos/sao-carlos/getting-ready.jpg =400x)  
Getting things ready for the event.

![Intro to GELOS](/blog/imagens/debian-day-30-anos/sao-carlos/gelos-intro.jpg =400x)  
Intro to GELOS talk.

![Debian intro](/blog/imagens/debian-day-30-anos/sao-carlos/debian-intro.jpg =400x)  
 A ~~not so~~ Brief Introdution to the Debian Project talk.

![Everyone knows Debian](/blog/imagens/debian-day-30-anos/sao-carlos/everyone-knows-debian.jpg =400x)  
Everyone already knew Debian!

![Free software](/blog/imagens/debian-day-30-anos/sao-carlos/free-culture.jpg =400x)  
Debian and the Free Culture talk

![Auditorium](/blog/imagens/debian-day-30-anos/sao-carlos/auditorium.jpg =400x)  
People in the auditorium space.  

![Free software](/blog/imagens/debian-day-30-anos/sao-carlos/free-software.jpg =400x)  
Free Software: the paths to a free life talk

![Coffee Break](/blog/imagens/debian-day-30-anos/sao-carlos/coffee-break.jpg =400x)  
Coffee Break.

![FOSS Ecosystem](/blog/imagens/debian-day-30-anos/sao-carlos/foss-ecosystem.jpg =400x)  
 The FOSS Ecosystem and You talk.

![Group photo](/blog/imagens/debian-day-30-anos/sao-carlos/group-photo.jpg =400x)  
Group photo.

![Celebration afterwards](/blog/imagens/debian-day-30-anos/sao-carlos/bar.jpg =400x)  
Celebration goes on in the bar.
