---
title: "Links da live Debian Brasil de 30/03/2021"
kind: article
created_at: 2021-03-31 19:00
author: Paulo Henrique de Lima Santana
---

A seguir estão os links citados na live do
[canal Debian Brasil no YouTube](https://www.youtube.com/debianbrasiloficial)
do dia 30 de março de 2021.

[Vídeo](https://www.youtube.com/watch?v=6xiOCL-R_oY) para assistir.

Participantes:

* [Daniel Lenharo](https://www.sombra.eti.br/)
* [Paulo Santana](https://phls.com.br)
* [Valéssio Brito](http://valessiobrito.com.br/)

Grupo [Debian Art & Design](https://t.me/debiandesign) no Telegram.

[Open Source Design](https://opensourcedesign.net)

Perfil [Pinguim Criativo no instagram](https://www.instagram.com/pinguimcriativo/)
que publica prints das áreas de trabalho enviados por usuários(as).

Produções do Valéssio:

* [Repositório no GitLab](https://gitlab.com/valessiobrito/artwork)
* [Arquivos](http://valessiobrito.com.br/projetos/)
* Debian-br-cdd [versão 1](https://static.vivaolinux.com.br/imagens/artigos/cdd_gdm.png) e
[versão 2](http://valessiobrito.com.br/projetos/debian/debian-br-cdd/gdm-logo_v2.svg)
* [Logo Debian Day](https://wiki.debian.org/Brasil/Eventos?action=AttachFile&do=get&target=DebianDay.png)
* Tema [Space Fun](https://wiki.debian.org/DebianArt/Themes/SpaceFun) 
para o Debian Squeeze lançado em 2011.
* [Logo Debian Diversidade](https://www.debian.org/logos/diversity-2019.png
)

[Palestra](https://archive.fosdem.org/2016/schedule/event/ian_murdock/)
do Martin Michlmayr sobre o Ian Murdock no FOSDEM 2016 e
[vídeo](https://peertube.debian.social/videos/watch/12bf0541-36dd-4f2d-8ee1-09b1ddbae112)
em homenagem a ele.

[Proposta de redesign](https://wiki.debian.org/KallesDesign)
do site debian.org

[Debian Art](https://wiki.debian.org/DebianArt)

Lista de discussão [design-devel](https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/design-devel)


[Concurso de logos](https://wiki.debconf.org/wiki/DebConf10/LogoContest)
da DebConf10

[MiniDebConf Online #2 "Gaming Edition"](https://mdco2.mini.debconf.org/)

[Libre Graphics Meeting (LGM)](https://libregraphicsmeeting.org)


