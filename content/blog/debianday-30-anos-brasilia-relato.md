---
title: "Debian Day 30 anos em Brasília"
kind: article
created_at: 2023-09-08 14:34
author: Aquila Macedo Costa, Thaís Rebouças de Araujo
---

O [Debian Day](https://wiki.debian.org/DebianDay/) deste ano foi muito especial, pois celebramos os 30 anos do Debian!
Dada a importância deste evento, a comunidade brasileira planejou uma
semana especial para celebração. Em vez de apenas encontros locais, tivemos uma semana de palestras online
transmitidas através do canal do [Debian no YouTube](https://www.youtube.com/playlist?list=PLU90bw3OpxLoiEUdWm7uyTi71ATGk6oXm) (em breve as gravações estarão
disponíveis na [instância PeerTube do Debian](https://peertube.debian.social/w/p/r1gZyzq3anFaECh6QEmU4d). No entanto, as celebrações locais
aconteceram em todo o país e organizamos um em Brasília na [Universidade de Brasília](https://fga.unb.br/) no campus Gama.

O evento aconteceu no dia 29 de agosto e durou toda a tarde. Durante esse periodo, realizamos duas
palestras para diferentes grupos de estudantes da universidade, a primeira palestra reuniu cerca de 30 alunos, seguida por outra palestra para uma turma de 80 estudantes.
Cada palestra teve uma duração média de aproximadamente 2 horas e abordou os seguintes temas:

- Debian e Comunidade do Debian
- Software livre

O Debian nos ajudou no coffee break onde tivemos a oportunidade de conversar com os participantes. O evento finalizou com uma foto de grupo, que você pode conferir, juntamente com muitas outras imagens, abaixo.

Além disso, promovemos o evento por meio de canais de comunicação como o Telegram dos cursos de tecnologia da Universidade de Brasília, garantindo que o maior número possível de alunos pudesse participar desta celebração única.

![Promotional folder](/blog/imagens/debian-day-30-anos/brasilia/folder.jpeg =400x)  

Fotos tiradas durante o evento:

![Photo 1](/blog/imagens/debian-day-30-anos/brasilia/present_debian_0.jpeg =400x)

![Photo 2](/blog/imagens/debian-day-30-anos/brasilia/present_debian_1.jpeg =400x)

![Photo 3](/blog/imagens/debian-day-30-anos/brasilia/present_debian_2.jpeg =400x)

Apresentação sobre o debian na primeira aula.

![Photo 1](/blog/imagens/debian-day-30-anos/brasilia/present_free_software_0.jpeg =400x)

![Photo 2](/blog/imagens/debian-day-30-anos/brasilia/present_free_software_1.jpeg =400x)

Apresentação sobre software livre em auditório.
