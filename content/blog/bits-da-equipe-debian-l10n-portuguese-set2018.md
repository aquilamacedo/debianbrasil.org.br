---
title: "BITS da equipe debian-l10n-portuguese - Set/2018"
kind: article
created_at: 2018-10-10 01:27
author: Daniel Lenharo de Souza
---

Mensalmente emitimos um relatório da
[equipe debian-l10n-portuguese](https://wiki.debian.org/Brasil/Traduzir) do
Projeto [Debian](https://www.debian.org) e este é o segundo deles. O objetivo é
que as atividades da equipe sejam conhecidas e possamos dar os devidos créditos
pelo trabalho produzido por nossos contribuidores. Acreditamos que isso poderá
estimular a participação de novos tradutores e revisores.

## Visão geral de setembro de 2018

-   01 Páginas traduzidas/atualizadas no site oficial.
-   05 Traduções PO.
-   05 Traduções de páginas wiki

## Pessoas com atividades registradas durante setembro de 2018

-   Adriana Costa
-   Adriano Rafael Gomes
-   Daniel Lenharo de Souza
-   Lia Ayumi Takiguchi
-   Paulo Henrique de Lima Santana
-   Qobi Ben Nun
-   Ricardo Fantin da Costa
-   William Moreira

Se você fez alguma colaboração de tradução que não está listada acima, nos
avise na lista (debian-l10n-portuguese@lists.debian.org) para que seu trabalho
seja devidamente registrado e reconhecido!

## Itens traduzidos

-   <https://wiki.debian.org/pt\_BR/ImapProxy>
-   <https://wiki.debian.org/pt\_BR/LDAPFormatNisMap>
-   <https://wiki.debian.org/pt\_BR/Debian>
-   <https://wiki.debian.org/pt\_BR/BridgeNetworkConnections>
-   <https://wiki.debian.org/pt\_BR/DataBase>
-   ddp://manuals/trunk/release-notes/issues.po
-   ddp://manuals/trunk/release-notes/old-stuff.po
-   ddp://manuals/trunk/release-notes/installing.po
-   ddp://manuals/trunk/release-notes/release-notes.po
-   po://debian-installer/level1/sublevel3/pt\_BR.po
-   wml://www.debian.org/CD/http-ftp/index.wml

A equipe de tradução para português do Brasil agradece a todos que colaboram
para que o Debian se torne mais universal!

Venha [ajudar](https://wiki.debian.org/Brasil/Traduzir) você também. Toda ajuda
é bem-vinda e estamos precisando da sua.