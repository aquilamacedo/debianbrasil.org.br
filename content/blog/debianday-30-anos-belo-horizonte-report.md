---
title: "Debian Day 30 years in Belo Horizonte - Brazil"
kind: article
created_at: 2023-08-24 23:00
author: Paulo Henrique de Lima Santana (phls)
---

For the first time, the city of Belo Horizonte held a
[Debian Day](https://wiki.debian.org/DebianDay/2023) to celebrate the
anniversary of the [Debian Project](https://www.debian.org).

The communities [Debian Minas Gerais](https://debian-minas-gerais.gitlab.io/site)
and [Free Software Belo Horizonte and Region](https://softwarelivre.bhz.br/)
felt motivated to celebrate this special date due the 30 years of the Debian
Project in 2023 and they organized a meeting on August 12nd in
[UFMG Knowledge Space](https://www.ufmg.br espacodoconhecimento/descubra/sala-multiuso/).

The Debian Day organization in Belo Horizonte received the important support
from [UFMG Computer Science Department](https://dcc.ufmg.br) to book the
room used by the event.

It was scheduled three activities:

  - Talk: The Debian project wants you! Paulo Henrique de Lima Santana
  - Talk: Customizing Debian for use in PBH schools: the history of Libertas - Fred Guimarães
  - Discussion: about the next steps to increase a Free Software community in BH - Bruno Braga Fonseca
  
In total, 11 people were present and we took a photo with those who stayed
until the end.

![Presentes no Debian Day 2023 em BH](/blog/imagens/debian30-belo-horizontel-phls_2023-08-12.jpg =400x)
