---
title: "MiniDebConf Brasília 2023 - 25 a 27 de maio"
kind: article
created_at: 2023-04-01 11:00
author: Lucas Kanashiro
---

Nesse ano a MiniDebConf Brasil está de volta! A comunidade brasileira de
usuários(as) e desenvolvedores(as) Debian convida a todos(as) a participarem da
[MiniDebConf Brasília 2023](https://brasilia.mini.debconf.org/) que acontecerá
durante 3 dias na capital federal.

Nos dias 25 e 26 de maio estaremos no Complexo Avançado da Câmara dos Deputados
-  LabHacker/CEFOR, promovendo palestras, oficinas e outras atividades. E, no
- dia 27 de maio (sábado), estaremos em um coworking (local a definir) para
- colocar a mão na massa hackeando software e contribuindo para o Projeto Debian.

A MiniDebConf Brasília 2023 é um evento aberto a todos(as), independente do seu
nível de conhecimento sobre Debian. O mais importante será reunir a comunidade
para celebrar o maior projeto de Software Livre do mundo, por isso queremos
receber desde usuários(as) inexperientes que estão iniciando o seu contato com o
Debian até Desenvolvedores(as) oficiais do projeto.

A programação da MiniDebConf Brasília 2023 será composta de palestras de nível
básico e intermediário para aqueles(as) participantes que estão tendo o primeiro
contato com o Debian ou querem conhecer mais sobre determinados assuntos,
e workshops/oficinas de nível intermediário e avançado para usuários(as) do
Debian que querem colocar a mão-na-massa durante o evento. No último dia do
evento, teremos um Hacking Day em um espaço de coworking para que todos possam
interagir e de fato fazerem contribuições para o projeto.

## Inscrição

A inscrição na MiniDebConf Brasília 2023 é totalmente gratuita e poderá ser
feita no [formulário](https://brasilia.mini.debconf.org/inscricao) disponível no
site do evento. É mandatória a inscrição prévia para que todos(as) possam
acessar a Câmara dos Deputados - devido a questões de segurança da casa. Além de
auxiliar a organização no dimensionamento do evento.

O evento é organizado de forma voluntária, e toda ajuda é bem-vinda. Portanto,
se você gostaria de contribuir para a realização do evento, preencha o
[formulário para inscrição de voluntários](https://brasilia.mini.debconf.org/voluntarios).
Os formulários para inscrição (na MiniDebConf e para ajudar na organização) e
[submissão de atividades](https://brasilia.mini.debconf.org/submissao-de-atividades)
serão abertos no dia 1º de abril (não tem nenhuma pegadinha, se inicia nesse dia
mesmo :)

## Contato

Para entrar em contato com o time local, a
[lista de emails debian-br-eventos](https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos),
o canal IRC #debian-bsb no OFTC e o
[grupo no telegram DebianBrasília](https://t.me/debianbrasilia) podem ser
utilizados. Além do endereço de email: <brasil.mini@debconf.org>.
