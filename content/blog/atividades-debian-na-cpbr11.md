---
title: "Atividades Debian na #CPBR11"
kind: article
created_at: 2018-01-28 17:35
author: Paulo Henrique de Lima Santana
---

Veja abaixo a relação de atividades Debian na Campus Party Brasil 2018 - CPBR11,
que acontecerá de 31 de janeiro a 4 de fevereiro de 2018 no Anhembi em São Paulo.

**Oficina para assinatura de chaves OpenPGP**

-   Giovani Ferreira (DD)
-   31 de janeiro às 18h
-   Bancada de Software Livre

**Oficina: Servidor web em Debian: HTML, PHP e MySQL na prática**

-   Wellton Costa
-   01 de fevereiro às 10h30min
-   Bancada de Software Livre

<https://campuse.ro/events/campus-party-brasil-2018/workshop/servidor-web-em-debian-html-php-e-mysql-na-pratica-cpbr11-comunidades-softwarelivre>

**Oficina: Ensinando e praticando o "transplante" de uma instalação Debian GNU/Linux**

-   Paulo Roberto Alves de Oliveira (Kretcheu) (DM)
-   02 de fevereiro às 15h
-   Bancada de Software Livre

<https://campuse.ro/events/campus-party-brasil-2018/workshop/ensinando-e-praticando-o-transplante-de-uma-instalacao-debian-gnulinux-cpbr11-comunidades-softwarelivre>

**Oficina: Debian no desktop, instalando e conhecendo as opções**

-   Daniel Lenharo (DD)
-   02 de fevereiro às 17h45min
-   Bancada de Software Livre

<https://campuse.ro/events/campus-party-brasil-2018/workshop/debian-no-desktop-instalando-e-conhecendo-as-opcoes-cpbr11-comunidades-softwarelivre>

**Painel: Debian - ouvindo experiências e contribuindo para o projeto**

-   Daniel Lenharo (DD) e Paulo Henrique Santana (DM)
-   03 de fevereiro às 00h
-   Palco Coders

<https://campuse.ro/events/campus-party-brasil-2018/talk/debian-ouvindo-experiencias-e-contribuindo-para-o-projeto-cpbr11>

**Workshop Contribuindo para o projeto Debian**

-   Daniel Lenharo (DD), Paulo Henrique Santana (DM), Lucas Kanashiro (DD)
-   03 de fevereiro às 10h30min
-   Workshop Multi

<https://campuse.ro/events/campus-party-brasil-2018/talk/workshop-contribuindo-para-o-projeto-debian>