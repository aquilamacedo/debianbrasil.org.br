---
title: "#FiqueEmCasaUseDebian palestras online sobre Debian de 3 a 30 de maio de 2020"
kind: article
created_at: 2020-05-08 17:50
author: Paulo Henrique de Lima Santana
---

A Comunidade Debian Brasil está realizando o evento chamado
**#FiqueEmCasaUseDebian** onde teremos uma palestra online por dia sobre Debian
realizada por membros da comunidade. Serão mais de 20 palestras!

De segunda a sexta-feira as palestras acontecerão às 20:00h (horário de
Brasília) e serão transmitidas no canal
[Debian Brasil no YouTube](https://www.youtube.com/channel/UCc6Yr77vh0vVh-yDXYwKLXg).
Aos sábados teremos atividades práticas de empacotamento às 14:00h.

Para ver a programação completa e mais informações, acesse:

<http://live.debianbrasil.org.br>

![](https://debianbrasil.gitlab.io/FiqueEmCasaUseDebian/assista.png =400x)


