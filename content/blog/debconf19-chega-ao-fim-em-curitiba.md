---
title: "DebConf19 chega ao fim em Curitiba"
kind: article
created_at: 2019-08-18 21:19
author: Paulo Henrique de Lima Santana
---

## DebConf19 chega ao fim em Curitiba

*Próxima edição da conferência vai acontecer em Israel. Evento já tem data definida.*

Luiz Fernando Hanysz | 28.07.2019

Neste sábado (27 de julho), a
[Conferência anual de Desenvolvedores(as) e Colaboradores(as) do Projeto Debian](https://debconf19.debconf.org/)
chegou ao fim. Realizada pela segunda vez no Brasil, a conferência contou com
145 palestras, workshops e atividades. Foram mais de 380 participantes, de 50
países diferentes.

Antes do início da Conferência, a tradicional DebCamp aconteceu entre 14 e 19 de
julho e focou na organização de infraestrutura e um workshop de empacotamento.
Ao longo de três dias, as atividades permitiram que novos colaboradores pudessem
iniciar o empacotamento Debian. Antonio Terceiro, 38, completa sua oitava
participação em DebConfs com a edição de 2019. Terceiro mora em Curitiba há
cinco anos e fez parte do time local da DebConf19. "Organizar é diferente porque
geralmente vamos à Conferência trabalhar com o Debian e conhecer novos lugares e
pessoas. Desta vez, me dediquei totalmente para a realização do evento".

O [Open Day](https://debconf19.debconf.org/news/2019-07-20-open-day/), realizado
no dia 20, contou com mais de 250 participantes. A programação envolveu
apresentações e workshops para o público em geral, uma feira de emprego com
stands dos patrocinadores da DebConf19 e um festival de instalação do sistema
Debian. Daniel Lenharo, 37, participou presencialmente do encontro pela segunda
vez. Lenharo acredita que o evento foi um sucesso. "Conseguimos deixar tudo
preparado com antecedência, então não tivemos grandes imprevistos", conta.

A grande Conferência de Desenvolvedores(as) Debian (DebConf19) teve início no
domingo (21). Junto com as tradicionais 'Bits from the DPL' e palestras
relâmpago, também foram realizadas demonstrações ao vivo e o anúncio da
[DebConf20](https://wiki.debian.org/DebConf/20), que será realizada em Haifa,
Israel. O evento também abriu espaço para sessões relacionadas ao lançamento
mais recente do Debian - versão 10, "Buster" - e os novos recursos do sistema.
As sessões também abordaram atualizações de projetos internos de equipes Debian,
além de eventos de interesse sobre o Debian e software livre em geral. A
[programação](https://debconf19.debconf.org/schedule/) foi diariamente
atualizada com atividades planejadas e ad-hoc, introduzidas pelos participantes
ao longo da Conferência.

Para quem não esteve em Curitiba, a maioria das palestras e sessões foram
transmitidas ao vivo e gravadas para o arquivo de reuniões, disponível
[aqui](https://ftp.acc.umu.se/pub/debian-meetings/2019/DebConf19/). Quase todas
as sessões facilitaram a participação remota por meio de aplicativos de
mensagens do IRC ou documentos de texto colaborativos on-line. O
[site da DebConf19](https://debconf19.debconf.org) permanecerá ativo para fins
de arquivamento e continuará oferecendo links para apresentações e vídeos de
palestras e eventos.

Jose Calhariz, 48, veio de Lisboa, Portugal. O Debian developer acredita que a
DebConf é importante para conhecer outras culturas e países. Perguntado sobre o
que achou do encontro em Curitiba, ele é breve e sincero: "Achei bom!". Calhariz
participou pela primeira vez de uma DebConf em 2007, na Argentina. De lá pra cá,
já perdeu as contas.

Durante a Conferência, várias equipes (*Front Desk*, equipe de boas-vindas e
equipe anti-assédio) estiveram disponíveis para que os participantes obtenham
sua melhor experiência na Conferência e encontrem soluções para qualquer
problema que possa surgir, tanto os que estiveram presentes em Curitiba, quanto
os remotos. Hamid Nassiby, 36, veio do Irã para participar da DebConf pela
primeira vez. O engenheiro de computação trabalha com plataformas livres desde
2001, participando de eventos relacionados ao Linux em seu país natal. Nassiby
elogiou a recepção que teve em Curitiba e na Conferência em geral. "Não me senti
um estrangeiro aqui. Estou muito feliz em participar da DebConf, o que foi bem
energizante para mim. O time local fez um grande trabalho, sempre perguntando
como eu me sentia, se estava com dificuldades. Os problemas foram poucos, e
foram resolvidos", comenta.

Veja a página da web sobre o
[Código de Conduta](https://debconf19.debconf.org/about/coc/) no site da
DebConf19 para mais detalhes.

Em 2020, a DebConf20 será realizada entre os dias 23 a 29 de agosto. Como já é
tradição, os organizadores locais em Israel iniciarão as atividades da
Conferência com a DebCamp (16 a 22 de agosto), com o objetivo de adequar a
infraestrutura necessária para o evento. O Debian agradece o empenho de inúmeros
patrocinadores para apoiar a DebConf19, particularmente nossos patrocinadores
Platinum: Infomaniak, Google e Lenovo.

**Sobre o Debian**

O [Projeto Debian](https://debian.org) foi fundado em 1993 por Ian Murdock para
ser um projeto comunitário verdadeiramente livre. Desde então, o projeto cresceu
e se tornou um dos maiores e mais influentes projetos de código aberto. Milhares
de voluntários do mundo todo trabalham juntos para criar e manter software
Debian. Disponível em 70 idiomas e suportando uma enorme variedade de tipos de
computadores, o Debian chama-se sistema operacional universal.

**Sobre a DebConf**

A DebConf é a conferência de desenvolvedores do Projeto Debian. Além de um
cronograma completo de palestras técnicas, sociais e políticas, a DebConf
oferece uma oportunidade para que desenvolvedores, colaboradores e outras
pessoas interessadas se encontrem pessoalmente e trabalhem em conjunto mais de
perto. A conferência ocorre anualmente desde 2000 em locais tão variados quanto
a Escócia, a Argentina e a Bósnia e Herzegovina. Mais informações sobre o
DebConf estão disponíveis em <https://debconf.org>.

**Sobre o Infomaniak**

A [Infomaniak](https://www.infomaniak.com) é a maior empresa de hospedagem na
web da Suíça, oferecendo também serviços de backup e armazenamento, soluções
para organizadores de eventos, transmissão ao vivo e serviços de vídeo sob
demanda. A empresa possui datacenters e todos os elementos essenciais para o
funcionamento dos serviços e produtos fornecidos (software e hardware).

**Sobre o Google**

O [Google](https://google.com) é uma das maiores empresas de tecnologia do
mundo, oferecendo uma grande variedade de serviços e produtos relacionados à
Internet, como tecnologias de publicidade on-line, pesquisa, computação em
nuvem, software e hardware.

O Google tem apoiado o Debian patrocinando a DebConf por mais de dez anos, e
também é um parceiro Debian que patrocina partes da infraestrutura de integração
contínua da Salsa no Google Cloud Platform.

**Sobre a Lenovo**

Como um líder global de tecnologia que fabrica um amplo portfólio de produtos
conectados, incluindo smartphones, tablets, PCs e estações de trabalho, bem como
dispositivos AR/VR, soluções inteligentes para casa/escritório e datacenter, a
[Lenovo](https://www.lenovo.com/br/pt/) entende como sistemas e plataformas
abertas críticos estão conectados ao mundo.

Informações de contato

Para mais informações, visite a página da web DebConf19 em
<https://debconf19.debconf.org> ou envie um e-mail para <press@debian.org>.

![800px debconf19 horizontal](/blog/imagens/800px-debconf19-horizontal.png  =400x)