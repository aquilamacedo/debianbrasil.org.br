---
title: "Debian Day 30 years in Curitiba - Brazil"
kind: article
created_at: 2023-08-24 20:00
author: Daniel Lenharo de Souza
---

As we all know, this year is a very special year for the Debian project, the
project turns 30!  
The Brazilian Community joined in and during the anniversary week, organized
some online activities through the
[Debian Brasil YouTube channel](https://www.youtube.com/DebianBrasilOficial).  
Information about talks given can be seen on the 
[commemoration website](https://debianbrasil.gitlab.io/debian30anos/).  
Talks are also have been published individually on the 
[Debian social Peertube](https://peertube.debian.social/w/p/r1gZyzq3anFaECh6QEmU4d)
and [Youtube](https://www.youtube.com/playlist?list=PLU90bw3OpxLoiEUdWm7uyTi71ATGk6oXm).  

After this week of celebration, the Debian Community in Curitiba, decided to
get together for a lunch for confraternization among some local members.  
The confraternization took place at
[The Barbers restaurant](https://osm.org/go/M_X_PHnQE?node=6177678698). The
local menu was a traditional Feijoada (Rice, Beans with pork meat,
fried banana, cabbage, orange and farofa). The meeting took place with a lot
of laughs, conversations, fun, caipirinha and draft beer!  

We can only thank the Debian Project for providing great moments!  

A small photographic record of the people present!  

![Group photo](/blog/imagens/DebianDayCuritiba2023.jpg =400x)  
