---
title: "BITS da equipe debian-l10n-portuguese - Ago/2018"
kind: article
created_at: 2018-09-01 14:28
author: Daniel Lenharo de Souza
---

Planejamos lançar mensalmente um relatório da
[equipe debian-l10n-portuguese](https://wiki.debian.org/Brasil/Traduzir) do
Projeto [Debian](https://www.debian.org/) e este é o primeiro deles. O objetivo
é que as atividades da equipe sejam conhecidas e possamos dar os devidos
créditos pelo trabalho produzido por nossos contribuidores. Acreditamos que
isso poderá estimular a participação de novos tradutores e revisores.

## Visão geral de agosto de 2018

Atualização da wiki sobre como traduzir as páginas web.

-   Traduções DDTP.
-   03 Páginas traduzidas/atualizadas no site oficial.
-   07 Traduções PO.
-   09 Traduções de páginas wiki
-   02 Traduções de posts da equipe de publicidade.

## Pessoas com atividades registradas durante agosto de 2018

-   Adriano Rafael Gomes
-   Barbara Tostes
-   Daniel Lenharo de Souza
-   Lia Ayumi Takiguchi
-   Paulo Henrique de Lima Santana (phls)
-   Qobi Ben Nun
-   Ricardo Fantin da Costa
-   Tássia Camoes Araujo
-   Willian Moreira

Se você fez alguma colaboração de tradução que não está listada acima, nos avise
na lista (debian-l10n-portuguese@lists.debian.org) para que seu trabalho seja
devidamente registrado e reconhecido!

## Conheça o trabalho na wiki

A wiki é dividida em 14 seções (mais uma com instruções para tradutores). A
ideia é fazer um "percurso em nível" traduzindo a primeira página de cada seção
e depois as páginas citadas diretamente em cada seção. As seções voltadas para
*Hardware* e *Instalação Rápida* já foram traduzidas, a maior seção em número de
artigos com link no começo da seção é a de *Serviços de Rede*. Todas as páginas
traduzidas nesse mês são da seção de *Serviços de Rede* e faltam mais 37 páginas
para terminar a seção antes de partir para a próxima.

Para se ter uma ideia do tamanho da wiki que deve ser traduzida observe uma
estatística geral do número de páginas existentes em algumas línguas. A contagem
foi feita com uma pesquisa pelo prefixo de cada língua no nome da página:

-   it = 756
-   fr = 720
-   ru = 443
-   es = 289
-   de = 210
-   pt\_BR = 120

É estimado o término das 37 páginas da seção de Serviços de Rede até o final do
ano. E dependendo do tamanho das outras páginas, passar de 250 páginas
traduzidas até a [DebConf19](https://debconf19.debconf.org/). Essa é uma
previsão otimista, mas talvez não tanto já que existem páginas sobre programas
com uma breve descrição possibilitando traduzir um número maior de páginas em
menos tempo.

## Itens traduzidos

-   Please review and translate blog post (and announcement) about DebConf18 closing
-   Please translate debian 25 years post
-   ddp://manuals/trunk/release-notes/upgrading.po
-   po-debconf://kstars-data-extra-tycho2/pt\_BR.po
-   po-debconf://mailman3-core/pt\_BR.po
-   po-debconf://miniupnpd/pt\_BR.po
-   po-debconf://nvidia-graphics-drivers375.82-4/ptBR.po
-   po://debian-installer/level1/sublevel1/pt\_BR.po
-   po://debian-installer/level2/tasksel/debian/po/pt\_BR.po
-   wml://www.debian.org/Bugs/index.wml
-   wml://www.debian.org/CD/index.wml
-   wml://www.debian.org/ports/hurd/index.wml
-   <https://wiki.debian.org/pt_BR/Apache>
-   <https://wiki.debian.org/pt_BR/WebServers>
-   <https://wiki.debian.org/pt_BR/DatabaseServers>
-   <https://wiki.debian.org/pt_BR/ConvertNetToStatic>
-   <https://wiki.debian.org/pt_BR/HowtoProxyThroughSSH>
-   <https://wiki.debian.org/pt_BR/NetworkManager>
-   <https://wiki.debian.org/pt_BR/SSH>
-   <https://wiki.debian.org/pt_BR/ConvertNetToStatic>
-   <https://wiki.debian.org/pt_BR/HowtoProxyThroughSSH>

A equipe de tradução para português do Brasil agradece a todos que colaboram
para que o Debian se torne mais universal!

Venha [ajudar](https://wiki.debian.org/Brasil/Traduzir) você também. Toda ajuda
é bem-vinda e estamos precisando da sua.