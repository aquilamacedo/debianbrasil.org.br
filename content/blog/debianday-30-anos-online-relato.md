---
title: "Debian Day 30 anos online no Brasil"
kind: article
created_at: 2023-08-25 16:00
author: Paulo Henrique de Lima Santana (phls)
---

Em 2023 o tradicional [Debian Day](https://wiki.debian.org/DebianDay/) está
sendo celebrado de forma especial, afinal no dia 16 de agostoo Debian completou
30 anos!

Para comemorar este marco especial na vida do Debian, a
[comunidade Debian Brasil](https://debianbrasil.org.br) organizou uma semana
de palestras online de 14 a 18 de agosto. O evento foi chamado de
[Debian 30 anos](https://debianbrasil.gitlab.io/debian30anos/).

Foram realizadas 2 palestras por noite, das 19h às 22h, transmitidas pelo canal
[Debian Brasil no YouTube](https://www.youtube.com/playlist?list=PLU90bw3OpxLoiEUdWm7uyTi71ATGk6oXm)
totalizando 10 palestras. As gravações já estão disponíveis também no canal
[Debian Brasil no Peertube](https://peertube.debian.social/w/p/r1gZyzq3anFaECh6QEmU4d).

Nas 10 atividades tivemos as participações de 9 DDs, 1 DM, 3 contribuidores(as). 
A audiência ao vivo variou bastante, e o pico foi na palestra sobre preseed com
o Eriberto Mota quando tivemos 47 pessoas assistindo.

Obrigado a todos(as) participantes pela contribuição que vocês deram para o
sucesso do nosso evento.

- Antonio Terceiro
- Aquila Macedo
- Charles Melara
- Daniel Lenharo de Souza
- David Polverari
- Eriberto Mota
- Giovani Ferreira
- Jefferson Maier
- Lucas Kanashiro
- Paulo Henrique de Lima Santana
- Sergio Durigan Junior
- Thais Araujo
- Thiago Andrade

Veja abaixo as fotos de cada atividade:

![Nova geração: uma entrevista com iniciantes no projeto Debian](/blog/imagens/debian-30-anos-01-nova-geracao.jpg =400x)  
Nova geração: uma entrevista com iniciantes no projeto Debian

![Instalação personalizada e automatizada do Debian com preseed](/blog/imagens/debian-30-anos-02-preseed.jpg =400x)  
Instalação personalizada e automatizada do Debian com preseed

![Manipulando patches com git-buildpackage](/blog/imagens/debian-30-anos-03-git-buildpackaged.jpg =400x)  
Manipulando patches com git-buildpackage

![debian.social: Socializando Debian do jeito Debian](/blog/imagens/debian-30-anos-04-debian-social.jpg =400x)  
debian.social: Socializando Debian do jeito Debian

![Proxy reverso com WireGuard](/blog/imagens/debian-30-anos-05-wireguard.jpg =400x)  
Proxy reverso com WireGuard

![Celebração dos 30 anos do Debian!](/blog/imagens/debian-30-anos-06-reuniao.jpg =400x)  
Celebração dos 30 anos do Debian!

![Instalando o Debian em disco criptografado com LUKS](/blog/imagens/debian-30-anos-07-luks.jpg =400x)  
Instalando o Debian em disco criptografado com LUKS

![O que a equipe de localização já conquistou nesses 30 anos](/blog/imagens/debian-30-anos-08-traducao.jpg =400x)  
O que a equipe de localização já conquistou nesses 30 anos

![Debian - Projeto e Comunidade!](/blog/imagens/debian-30-anos-09-projeto-e-comunidade.jpg =400x)  
Debian - Projeto e Comunidade!

![Design Gráfico e Software livre, o que fazer e por onde começar](/blog/imagens/debian-30-anos-10-design-grafico.jpg =400x)  
Design Gráfico e Software livre, o que fazer e por onde começar




