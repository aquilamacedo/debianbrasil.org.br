---
title: "Grupos Locais Brasileiros no IRC"
kind: article
created_at: 2020-12-17 14:17
author: Daniel Lenharo
---

Com a recente articulação dos grupos locais Brasileiros, os grupos locais estão
buscando manter seu alinhamento com a forma de comunicação que o projeto Debian
já se utiliza.
Desta forma os grupos estão utilizando além dos grupos no telegram, utilizando
canais no IRC também!

Segue a lista dos canais recém criados!

\#debian-bsb  
\#debian-mg  
\#debian-sp  

Todos os canais estão no servidor irc.debian.org (ou OFTC)

O pessoal de Brasília fez um post no site deles recentemente, explicando como
se conectar ao IRC utilizando o element, assim fica mais prática para quem
prefere fazer uso do aparelho de celular.

[Clique aqui para ler o
post](https://debianbrasilia.org/irc/element/2020/12/06/irc-element.html)

Esperamos que em breve possamos ter novos grupos locais se articulando e
buscando realizar atividades em suas regiões!
