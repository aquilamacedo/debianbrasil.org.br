---
title: "Meeting with Dr Juliana Dias Bastos discussed brazilian translation work"
kind: article
created_at: 2021-05-18 20:00
author: Thiago Pezzo e Paulo Henrique de Lima Santana
---

Debian's Brazilian Portuguese [localization team](https://wiki.debian.org/Brasil/Traduzir)
(l10n.debian.org.br) carried out a virtual meeting on May 10th to discuss the
translation work that has being developed.

The thematic issues included diversity (the Brazilian Portuguese has two
grammatical genders which, in general, are strong marked) and equivalences
among technical terms from Portuguese and other languages.

Dr. Juliana Dias Bastos, PhD in Arts/Translation, was invited to lecture from
an academic perspective and to participate in the discussions.

We also had a most grateful presence of LibreOffice's pt-BR translation team,
which enriched our thoughts. Thank you all for your participation!