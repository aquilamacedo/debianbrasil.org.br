---
title: "Debian Day 30 anos no IF Sul de Minas, Pouso Alegre"
kind: article
created_at: 2023-08-24 10:00
author: Thiago Pezzo (Tico)
---
por [Thiago Pezzo](https://contributors.debian.org/contributor/tico@salsa/),
contribuidor Debian, equipe de localização pt_BR

O [Dia do Debian](https://wiki.debian.org/DebianDay/) deste ano é muito especial, estamos comemorando 30 anos!
Dada a importância deste evento, a comunidade brasileira planejou uma semana
muito especial. Em vez de apenas fazer as reuniões dos grupos locais, tivemos
uma semana de conversas on-line transmitidas pelo
[canal do YouTube](https://www.youtube.com/playlist?list=PLU90bw3OpxLoiEUdWm7uyTi71ATGk6oXm) do Debian
Brasil (e logo as gravações serão enviadas para a
[instância peertube](https://peertube.debian.social/w/p/r1gZyzq3anFaECh6QEmU4d)
da nossa equipe).

As celebrações locais aconteceram em todo o país e uma delas foi
organizada em [Pouso Alegre, MG, Brasil](https://www.openstreetmap.org/relation/315431),
no [Instituto Federal de Educação, Ciência e Tecnologia do Sul de Minas Gerais](https://portal.ifsuldeminas.edu.br/index.php)
(IFSULDEMINAS). O Instituto, como muitos de seus semelhantes pelo Brasil,
especializa-se em currículos profissionais e tecnológicos para o Ensino Médio e
Superior. Uma educação pública, gratuita e de qualidade!

O evento aconteceu na tarde do dia 16 de agosto no campus de Pouso Alegre.
Cerca de 30 estudantes do curso Técnico em Informática participaram da
apresentação sobre o [Projeto Debian](https://www.debian.org/) e sobre o
Movimento Software Livre em geral. Foi muito bom! E depois ainda tivemos algum
tempo livre para conversar.

Gostaria de agradecer a todas as pessoas que nos ajudaram:

- Professores Michelle Nery e Ismael David Muro (IFSULDEMINAS Pouso Alegre)
- Virgínia Cardoso e Melissa de Abreu (IFSULDEMINAS Reitoria)
- [Giovani Ferreira](https://nm.debian.org/person/giovani/) (um DD residente em Minas Gerais)
- Felipe Maia (do Debian São Paulo - obrigado pelos pôsteres!)
- Gustavo (um jovem e perspicaz estudante que fez comentários importantes sobre acessibilidade - obrigado!)
- E obrigado especial a todas e todos estudantes que participaram da apresentação, espero vê-los/as novamente!

Aqui vai a nossa foto de grupo:
![Apresentação no IFSULDEMINAS campus Pouso Alegre](/blog/imagens/ifsuldeminas.png =400x)

