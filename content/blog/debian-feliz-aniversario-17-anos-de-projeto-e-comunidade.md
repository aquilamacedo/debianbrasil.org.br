---
title: "Debian, Feliz Aniversário! 17 anos de projeto e comunidade!"
kind: article
created_at: 2010-08-16 12:00
author: Valessio Brito
---

Cartão feito por mim utilizando [Inkscape](http://www.inkscape.org) e
alguns cliparts livre do [OpenClipart.org](http://www.openclipart.org).

![](/blog/imagens/cardebianptbr.jpg)


Participe também do viral "Balões do Debian em seu site", ~~acesse
aqui~~ (*link não funciona mais*) e cole o código em seu blog ousite.
