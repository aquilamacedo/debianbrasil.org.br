---
title: "Encontro com Sheila Gomes discutiu o trabalho de tradutora"
kind: article
created_at: 2021-07-13 09:00
author: Thiago Pezzo e Paulo Henrique de Lima Santana
---

A [equipe de localização](https://wiki.debian.org/Brasil/Traduzir) do Debian
para português do Brasil (l10n.debian.org.br) realizou mais um bate-papo no dia
5 de julho de 2021. A convidada desta vez foi a tradutora e desenvolvedora
[Sheila Gomes](https://sheilagomes.medium.com/).

A partir de sua perspectiva, trabalhando entre tradução e desenvolvimento de
software, debatemos sobre diferenças entre tradução profissional e tradução por
comunidades de software livre, e as práticas entre grupos como
[Debian](https://www.debian.org/), [OmegaT](https://omegat.org) e
[WordPress](https://wordpress.org/). Entre outras sugestões, a Sheila nos
orientou a adequar a linguagem ao público e aos objetivos de cada tradução, ao
mesmo tempo buscando simplicidade na escrita.

Também nos explicou brevemente sobre o [OmegaT](https://omegat.org/), um
software livre de apoio aos(às) tradutores(as), que pode ser utilizado de forma
distribuída e poderia enriquecer o trabalho de nossa equipe. A Sheila também
participa da equipe de tradução do OmegaT e produz
[conteúdo de qualidade](https://medium.com/omegat) e em português sobre esta
ferramenta, uma das poucas fontes on-line sobre o aplicativo.

Novamente tivemos a presença de participantes de outras equipes de tradução
(LibreOffice, Krita, GNU, GNOME). Agradecemos a visita! E em especial,
agradecemos à Sheila por ter participado e compartilhado seus conhecimentos e
vivências com a comunidade Debian.

Abraços e até a próxima!