---
title: "MiniDebConf Curitiba 2018 - a brief report"
kind: article
created_at: 2018-06-19 01:17
author: Adriana Costa
---

![Minidc19 banner grande](/blog/imagens/minidc19-banner-grande.jpg =400x)

The [MiniDebConf Curitiba third edition](https://minidebconf.curitiba.br/2018/en/)
was held from April 11th to 14th, 2018 at Campus Curitiba of the
[Federal University of Technology - Paraná (UTFPR)](http://www.utfpr.edu.br/estrutura-universitaria/pro-reitorias/prorec/diretoria-de-relacoes-interinstitucionais/international-affairs-eng/about-us).

In this edition besides the regular schedule, we organized on Sunday a barbecue
party for lunch (with vegetarian options too). So, it was a great opportunity to
meet people and to talk about Debian. Moreover, for the first time we had 3
foreign attendees/speakers and 2 of them are DD: Jon maddog Hall, Holger Levsen
nd Stefano Rivera. It was very helpful, we could asked them about online
transmission questions and some ideas for DebConf 2019. We are looking forward
to see all the Debian Community here next year! :-)

![Minidc19 churrasco](/blog/imagens/minidc19-churrasco.jpg =400x)

## Data about 2018 edition

During four days, many activities happened for every level of knowledge about
the Debian Project. In this edition, we had on the first two days just hands on
session similar to [DebCamp](https://wiki.debconf.org/wiki/DebConf17/DebCamp).
This experience was very interested and we believe that is a great way to
contribute with [Debian Project](http://debian.org). The schedule had:

-   13 talks
-   07 lightning talks
-   04 workshops
-   02 hands on session
-   21 KSP participants
-   07 bugs solved
-   Commits and merges to Salsa
-   02 Libreboot install
-   06 packaging uploaded.

The final numbers of the MiniDebConf Curitiba are the follow:

-   299 had completed registration
-   139 attended in total
    -   14 organizares
    -   06 volunteers
    -   08 DD
    -   04 DM
    -   01 new request for DM

On the closing session, It was done a call to people send logo
[proposals](https://wiki.debian.org/DebConf/19/Artwork/LogoProposals) to DebConf19.

## Women’s participation

The number of lectures done by women increased from 0 in 2017 to 5 this year
(with 4 speakers: Adriana Cássia da Costa, Helen Koike, Miriam Retka and Renata
D'Avila). We have been working to have diversity since the first edition and we
have been learning in each edition. We will keep working hard, so we strongly
believe that we will improve that approach in the next editions.

![Minidc19 mulheres](/blog/imagens/minidc19-mulheres.jpg =400x)

## Crowdfunding

For the second time, we made a
[crowdfunding](https://minidebconf.curitiba.br/2018/en/donate/) and got funds to
pay some things (breakfast, venue, press, badge, lanyards). The crowdfunding goal
was R$ 3.000,00 and we got R\$ 3.940,00, we received from 62 donors from Brazil
and from other countries.

Each participant received: a badge, a customized lanyard, a flyer with tips
"how to contribute with Debian Project", and flyers from our sponsors.

![Minidc19 banner doadores](/blog/imagens/minidc19-banner-doadores.jpg =400x)

## Photos and videos

We made the lecture online transmission and we also recorded the lecture to
share before the event on the Debian repository. You can watch the lecture on
the following link (they are in pt-br):

<https://meetings-archive.debian.net/pub/debian-meetings/2018/mini-debconf-curitiba>

You can see the photos on the following link:

<https://www.flickr.com/photos/curitibalivre/sets/72157690015019780>

## Acknowledgment

We would like to give many thanks for all the participants that made the
MiniDebConf Curitiba 2018 a successful event.

We thank the speakers so much that made a big presentation, the participants
that participated a lot to the activities and contributed to the discussions
with their opinion, and the volunteers that gave their best to help in the event.

We thank the professors and employees from the Federal University of Technology
- Paraná (UTFPR), that received us very well and the venue space.

We thank our sponsors: [Collabora](https://www.collabora.com/),
[Alambra Eidos,](http://wwww.alhambra-eidos.com.br/)
[4Linux](http://www.4linux.com.br/) and
[Polilinux](http://www.polilinux.com.br/) that believed in our event.

**We hope that after the MiniDebConf Curitiba 2018 more people will be inspired to contribute with the Debian Brazil Community.**

![Minidc19 todos](/blog/imagens/minidc19-todos.jpg)

![Minidc19 sponsors](/blog/imagens/minidc19-sponsors.png)