---
title: "Páginas do Debian Women traduzidas para português"
kind: article
created_at: 2016-11-16 00:30
author: Adriana Costa
---

As páginas do projeto Debian Women foram recentemente traduzidas para o
português em um esforço coletivo do time de tradução do Debian no Brasil.

Esperamos que isso também incentive mais mulheres brasileiras a participarem do
projeto.

Acesse em <https://www.debian.org/women/>

![Banner garota debian](/blog/imagens/banner-garota-debian.png =400x)

