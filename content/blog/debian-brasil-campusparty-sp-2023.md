---
title: "Participação do Debian na Campus Party Brasil 2023"
kind: article
created_at: 2023-08-02 10:00
author: Daniel Lenharo
---

Mais uma edição da [Campus Party Brasil](https://brasil.campus-party.org/cpbr15/)
aconteceu na cidade de São Paulo entre os dias 25 e 30 de Julho de 2023.
Novamente a comunidade Debian Brasil se fez presente. Durante os dias no espaço
disponibilizado, realizamos algumas atividades:  
  - Distribuição de brindes (adesivos, copos, cordão de crachá);  
  - Mini oficina sobre como contribuir para a equipe de tradução;  
  - Mini oficina sobre empacotamento;  
  - Assinatura de chaves;  
  - Informações sobre o projeto;  

Durante todos os dias, havia sempre uma pessoa disponível para passar
informações sobre o que é o Debian e as diversas formas de contribuir. Durante todo o
evento, estimamos que ao menos 700 pessoas interagiram de alguma forma com
nossa comunidade. Diversas pessoas, aproveitaram a oportunidade para
aproveitar pelo excelente trabalho realizado pelo projeto no Debian 12 -
Bookworm.  

Segue algumas fotos tiradas durante o evento!  

![CPBR15](/blog/imagens/cpbr15/CPBR15-01.jpg =400x)  
Espaço da Comunidade no Evento.  
![CPBR15](/blog/imagens/cpbr15/CPBR15-03.jpg =400x)  
Romulo, visitante do espaço com Daniel Lenharo.  
![CPBR15](/blog/imagens/cpbr15/CPBR15-04.jpg =400x)  
Alguns brindes que estavam a disposição do público.  
![CPBR15](/blog/imagens/cpbr15/CPBR15-05.jpg =400x)  
Visão do espaço.  
![CPBR15](/blog/imagens/cpbr15/CPBR15-06.jpg =400x)  
Adesivo com a Arte de 30 anos feita pelo Jefferson.  
![CPBR15](/blog/imagens/cpbr15/CPBR15-08.jpg =400x)  
Pessoal no espaço da comunidade.  
![CPBR15](/blog/imagens/cpbr15/CPBR15-10.jpg =400x)  
Mini curso de empacotamento, realizado pelo Charles.  
![CPBR15](/blog/imagens/cpbr15/CPBR15-09.jpg =400x)  
Pessoal que esteve envolvido nas atividades da comunidade.  
