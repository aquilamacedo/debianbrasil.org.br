---
title: "Bits das festas de lançamento do Bullseye e das celebrações do aniversário do Debian no Brasil em 2021"
kind: article
created_at: 2021-08-17 10:00
author: Paulo Henrique de Lima Santana
---

Este ano tivemos duas tradicionais comemorações do Projeto Debian acontecendo
praticamente juntas em agosto, em um intervalo de apenas 3 dias.

## Release Party

![Banner 28o debian day](/blog/imagens/banner-bullseye.png)

A primeira comemoração foi o lançamento do Debian 11 codinome "Bullseye" no dia
14 de agosto de 2021. Várias comunidades locais realizaram atividades pelo mundo
chamadas de [Release Party](https://wiki.debian.org/ReleasePartyBullseye) para
celebrar a chegada da nova versão do Debian.

No Brasil também tivemos atividades online que você pode assistir as gravações
no YouTube. Veja a relação abaixo:

Palestra do DD Paulo Henrique de Lima Santana:

* [O que é o Debian - uma visão geral sobre o sistema operacional universal](https://www.youtube.com/watch?v=fCW8kExhKJo)

Palestra do DD Daniel Lenharo:

* [Migrando do Buster para o Bullseye! Cuidados a serem tomados](https://www.youtube.com/watch?v=WOFRvWzEPo4)

Palestra do DD Eriberto Mota:

* [Atualização para o Debian 11 Bullseye via linha de comando](https://www.youtube.com/watch?v=5VtX6LQFTpE)

[Debianópolis](https://debxp.org/debianopolis2021)

* [Abertura](https://www.youtube.com/watch?v=zCYJs5M6tFg) Blau e Kretcheu
* [Territórios Digitais Livres](https://www.youtube.com/watch?v=KBoDxEmG6c0) Casa de Cultura Tainã
* [Debian Package Tracker](https://www.youtube.com/watch?v=zPjtiU6VeVQ) João Eriberto Mota Filho
* [FAQ Debian](https://www.youtube.com/watch?v=P_aJodFUidg) Paulo Kretcheu
* [O professor de Debianópolis](https://www.youtube.com/watch?v=QYgveO7KUUM) Ramon Mulin
* [O Debian é Livre!](https://www.youtube.com/watch?v=TTDBvUg5oKU) Blau Araujo
* [Intercooperativas](https://www.youtube.com/watch?v=CHkE4DrWKuM) Lívia Gouvea
* [Palestras Relâmpago](https://www.youtube.com/watch?v=BMPBd7wtPsA) Membros da comunidade

## Debian Day

![Banner 28o debian day](/blog/imagens/banner-28-debianday.png)

No dia 16 de agosto 2021 aconteceu a celebração do 28º aniversário do Debian
e novamente várias comunidades locais realizaram atividades pelo mundo chamadas
de [DebianDay](https://wiki.debian.org/DebianDay/2021).

No Brasil tivemos uma [live da comunidade debxp](https://www.youtube.com/watch?v=1b6OPyi3W84)
e um pequeno encontro de alguns membros da comunidade Debian Curitiba em uma
pizzaria (foto abaixo).

![Foto debian day curitiba](/blog/imagens/debianday-curitiba-2021.jpg)

Esperamos que em 2022 possamos voltar a nos encontrar pessoalmente
sem medo para celebrar mais um aniversário do Debian.

## Outros registros

Se você tiver registros de outras festas de lançamento ou comemoração do
aniversário do Debian no Brasil, por favor entre em contato conosco para
publicarmos aqui no site.

Nosso email para contato é: contato@debianbrasil.org.br
