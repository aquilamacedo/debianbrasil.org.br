---
title: "MiniDebConf Curitiba in 2016 was a success!"
kind: article
created_at: 2016-03-09 17:40
---

![MiniDebConf Curitiba 2016](/blog/imagens/2016minidebcwb_media.jpg)


Dear Friends,

The [MiniDebConf Curitiba 2016](http://br2016.mini.debconf.org/) was a success!
We had over two days of dedication to [Debian Project](https://www.debian.org/)
20 hours of programming. There were 85 people present enjoying 12 lectures,
07 Lightning Talks and 05 Workshops.

The organization has a great joy and satisfaction, we would like to thank the
important participation, presence and trust from each you.

We would like to thanks the speakers for prepare the lectures and transmit yours
knowledge.

We thank each one of the participants who believed in the event , attended and
contributed to be closer to the Debian community.

We thank the [Aldeia Coworking](http://aldeiacoworking.com.br/) for graciously
give us space and contribute to the event.

Last but not least, we thank the local team dedicated to working hard morning,
afternoon, evening and night, months ago, weeks before, days before the start
event. Each member of this team exceeded limits, exceeded expectations, seeking
the smile on his face with the exercise of solidarity in solving any problem
that presented itself. We hope that, somehow, the MiniDebConf Curitiba in 2016
may have contributed to its growth and professional person.

The organization invites all those involved in this event to invest energy in
continuing the collaborative spirit and strengthen community Debian and Free
Software in Brazil and World.

Thank you!!