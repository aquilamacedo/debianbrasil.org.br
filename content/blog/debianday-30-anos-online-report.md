---
title: "Debian Day 30 years online in Brazil"
kind: article
created_at: 2023-08-25 16:00
author: Paulo Henrique de Lima Santana (phls)
---

In 2023 the traditional [Debian Day](https://wiki.debian.org/DebianDay/) is
being celebrated in a special way, after all on August 16th Debian turned 30
years old!

To celebrate this special milestone in the Debian's life, the
[Debian Brasil community](https://debianbrasil.org.br) organized a week with
talks online from August 14th to 18th. The event was named
[Debian 30 years](https://debianbrasil.gitlab.io/debian30anos/).

Two talks were held per night, from 7:00 pm to 10:00 pm, streamed on the
[Debian Brasil channel on YouTube](https://www.youtube.com/playlist?list=PLU90bw3OpxLoiEUdWm7uyTi71ATGk6oXm)
totaling 10 talks. The recordings are also available on the
[Debian Brazil channel on Peertube](https://peertube.debian.social/w/p/r1gZyzq3anFaECh6QEmU4d).

We had the participation of 9 DDs, 1 DM, 3 contributors in 10 activities.
The live audience varied a lot, and the peak was on the preseed talk with
Eriberto Mota when we had 47 people watching.

Thank you to all participants for the contribution you made to the success of
our event.

- Antonio Terceiro
- Aquila Macedo
- Charles Melara
- Daniel Lenharo de Souza
- David Polverari
- Eriberto Mota
- Giovani Ferreira
- Jefferson Maier
- Lucas Kanashiro
- Paulo Henrique de Lima Santana
- Sergio Durigan Junior
- Thais Araujo
- Thiago Andrade

Veja abaixo as fotos de cada atividade:

![Nova geração: uma entrevista com iniciantes no projeto Debian](/blog/imagens/debian-30-anos-01-nova-geracao.jpg =400x)  
Nova geração: uma entrevista com iniciantes no projeto Debian

![Instalação personalizada e automatizada do Debian com preseed](/blog/imagens/debian-30-anos-02-preseed.jpg =400x)  
Instalação personalizada e automatizada do Debian com preseed

![Manipulando patches com git-buildpackage](/blog/imagens/debian-30-anos-03-git-buildpackaged.jpg =400x)  
Manipulando patches com git-buildpackage

![debian.social: Socializando Debian do jeito Debian](/blog/imagens/debian-30-anos-04-debian-social.jpg =400x)  
debian.social: Socializando Debian do jeito Debian

![Proxy reverso com WireGuard](/blog/imagens/debian-30-anos-05-wireguard.jpg =400x)  
Proxy reverso com WireGuard

![Celebração dos 30 anos do Debian!](/blog/imagens/debian-30-anos-06-reuniao.jpg =400x)  
Celebração dos 30 anos do Debian!

![Instalando o Debian em disco criptografado com LUKS](/blog/imagens/debian-30-anos-07-luks.jpg =400x)  
Instalando o Debian em disco criptografado com LUKS

![O que a equipe de localização já conquistou nesses 30 anos](/blog/imagens/debian-30-anos-08-traducao.jpg =400x)  
O que a equipe de localização já conquistou nesses 30 anos

![Debian - Projeto e Comunidade!](/blog/imagens/debian-30-anos-09-projeto-e-comunidade.jpg =400x)  
Debian - Projeto e Comunidade!

![Design Gráfico e Software livre, o que fazer e por onde começar](/blog/imagens/debian-30-anos-10-design-grafico.jpg =400x)  
Design Gráfico e Software livre, o que fazer e por onde começar




