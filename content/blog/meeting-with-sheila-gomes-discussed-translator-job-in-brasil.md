---
title: "Meeting with Sheila Gomes discussed translator job in Brasil"
kind: article
created_at: 2021-07-13 09:00
author: Thiago Pezzo e Paulo Henrique de Lima Santana
---

Debian's Brazilian Portuguese [localization team](https://wiki.debian.org/Brasil/Traduzir)
(l10n.debian.org.br) held another online meeting on July, 5th 2021. Our guest
this time was the translator and developer
[Sheila Gomes](https://sheilagomes.medium.com/).

Working with translation and software development, we debated about professional
translation and free software communities' translation, and about practices
among Brazilian groups like [Debian](https://www.debian.org/),
[OmegaT](https://omegat.org) and [WordPress](https://wordpress.org/). Amidst
several hints, Sheila recommended choosing terms accordingly to each text's
public and objectives, and at the same time, reaching for simplicity in writing.

She also explained quickly about [OmegaT](https://omegat.org/), a free software
designed to aid translators, which can be used in distributed contexts and could
improve our team's work. Sheila also takes part in the translation of OmegaT and
creates [quality content](https://medium.com/omegat) in Brazilian Portuguese
about this tool, one of the few sources available online about it.

Again, we had the presence of members from other Brazilian localization groups
(LibreOffice, Krita, GNU, GNOME). Thank you for your visiting! We would like to
sincerely thank Sheila for her participation and for sharing her knowledge and
experiences with our Debian community.

See you all next time!